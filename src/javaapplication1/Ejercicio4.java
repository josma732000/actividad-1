package javaapplication1;
import java.util.Scanner;

//4.-Crear un Arreglo de 10 posiciones y llenarlo con numeros del 1 al 10,
//sacar su suma y promedio
public class Ejercicio4 {
    
public static void main(String[] args) {
    
Scanner sc = new Scanner(System.in);
int i;
int pos = 0, neg = 0;
int[] numeros = new int[10];
double sumaPos = 0, sumaNeg = 0;
System.out.println("Jose Maria Andres Uc May 63829");
System.out.println("Lectura de los elementos de la matriz: ");
for (i = 0; i<10; i++) {
System.out.print("numeros["+ i + "]= ");
numeros[i]=sc.nextInt();
}
for (i = 0; i<10; i++) {
if (numeros[i]> 0){
sumaPos += numeros[i];
pos++;
} else if (numeros[i]< 0){
sumaNeg += numeros[i];
neg++;
}
}
if (pos != 0) {
System.out.println("Media de los valores positivos:" + sumaPos / pos);
} else {
System.out.println("No ha introducido numeros positivos");
}
if (neg != 0) {
System.out.println("Media de los valores negativos: " + sumaNeg / neg);
} else {
System.out.println("No ha introducido numeros negativos");

       }
   }
}