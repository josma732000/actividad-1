/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.util.Scanner;

/**
 *
 * @author Jose
 */

//5.-Genere un metodo que reciba un parametro del 1 al 7 y cree un arreglo que tenga 
//los nombres de los dias de la semana, dependiendo del parametro proporcionado 
//retornar el valor del dia de la semana 
public class Ejercicio5{
    public Ejercicio5(){
        System.out.println("Jose Maria Andres Uc May 63829");
        Scanner sc = new Scanner(System.in);
        int Dat;
        System.out.println("EJERCICIO 5");
        System.out.println("Ingrese un número del 1 al 7");
        System.out.println("Nota:Considere que el dia Domingo comienza con el 1");
        Dat = sc.nextInt();
        String[] día = {"Domingo", "Lunes", "Martes", 
                           "Miércoles", "Jueves", "Viernes",
                           "Sábado"};
        if(Dat < 8 && Dat > 0){
            System.out.println(día[Dat- 1]);
        }else{
            System.out.println("El número " + Dat + " esta fuera del rango");
        }
        System.out.println("........................................");
   
        
    }
} 
