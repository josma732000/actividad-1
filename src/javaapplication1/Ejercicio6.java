/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author Jose
 */

//6.-Imprima utilizando los dos tipos de ciclos for el arreglo de dos dimensiones 
//de los personajes de Star War
 public class Ejercicio6{
        public Ejercicio6(){
        System.out.println("EJERCICIO 6");
        System.out.println("Jose Maria Andres Uc May 63829");
        String STRW [][] = new String [4][4];
        STRW [0][0]= "Luke Skywalker";
        STRW [0][1]= "R2-D2";
        STRW [0][2]= "C-3PO";
        STRW [0][3]= "Darth Vader";
        STRW [1][0]= "Leia Organa";
        STRW [1][1]= "Owen Lars";
        STRW [1][2]= "Beru Whitesun Lars";
        STRW [1][3]= "R5-D4";
        STRW [2][0]= "Biggs Darklighter";
        STRW [2][1]= "Obi-Wan Kenobi";
        STRW [2][2]= "Yoda";
        STRW [2][3]= "Jek Tono Porkins";
        STRW [3][0]= "Jabba Desilijic Tiure";
        STRW [3][1]= "Han Solo";
        STRW [3][2]= "Chewbacca";
        STRW [3][3]= "Anakin Skywalker";
        
        System.out.println("\nPersonajes de Star Wars (ciclo for simple): ");
        for(int i=0;i<4;i++){
        System.out.println(java.util.Arrays.toString(STRW[i]));
        }
        
        System.out.println("\nPersonajes de Star Wars (ciclo for each): ");
        for(String[] i:STRW){
        System.out.println(java.util.Arrays.toString(i));}
        
        
        System.out.println("...................................................");
             }
         }
